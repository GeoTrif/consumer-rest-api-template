package com.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerRestApiTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerRestApiTemplateApplication.class, args);
	}

}

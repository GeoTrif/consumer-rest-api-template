package com.template.controller;

import com.template.newtrackon.client.NewTrackonClient;
import com.template.weatherstack.client.WeatherStackClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ConsumerRestApiController {

    private static final String INDEX_VIEW_NAME = "index";
    private static final String CITY_NAME = "Cluj Napoca";
    private static final String TRACKER_URLS_ATTRIBUTE_NAME = "trackerUrls";
    private static final String WEATHER_STACK_ATTRIBUTE_NAME = "weatherStack";

    private final NewTrackonClient newTrackonClient;
    private final WeatherStackClient weatherStackClient;

    public ConsumerRestApiController(NewTrackonClient newTrackonClient, WeatherStackClient weatherStackClient) {
        this.newTrackonClient = newTrackonClient;
        this.weatherStackClient = weatherStackClient;
    }

    @GetMapping()
    public String getIndexPage(Model model) {
        model.addAttribute(TRACKER_URLS_ATTRIBUTE_NAME, newTrackonClient.getTrackersPayload());
        model.addAttribute(WEATHER_STACK_ATTRIBUTE_NAME, weatherStackClient.getWeatherStackPayload(CITY_NAME));

        return INDEX_VIEW_NAME;
    }
}

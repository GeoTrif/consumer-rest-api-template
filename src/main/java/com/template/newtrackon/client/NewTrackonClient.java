package com.template.newtrackon.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.newtrackon.model.TrackerInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Component
public class NewTrackonClient {

    private static final String TRACKER_PAYLOAD_SPLIT_REGEX = "\n\n";

    private ObjectMapper objectMapper = new ObjectMapper();

    @Value("${newtrackon.api.url}")
    private String newTrackonUrl;

    public List<TrackerInfo> getTrackersPayload() {
        /*
        If you need more configs like basic authentication,use RestTemplateBuilder.

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        restTemplateBuilder.basicAuthentication("user", "password").build();
         */

        RestTemplate restTemplate = new RestTemplate();
        String trackersJsonPayload = restTemplate.getForObject(newTrackonUrl, String.class);

        String[] trackerUrls = trackersJsonPayload.split(TRACKER_PAYLOAD_SPLIT_REGEX);

        return transformToTrackerInfo(trackerUrls);
    }

    private List<TrackerInfo> transformToTrackerInfo(String[] trackerUrls) {
        List<TrackerInfo> trackerInfos = new ArrayList<>();

        // Getting only the first 10 entries.Change for loop for other results.
        for (int i = 1; i <= 10; i++) {
            trackerInfos.add(new TrackerInfo((long) i, trackerUrls[i]));
        }

        return trackerInfos;
    }
}

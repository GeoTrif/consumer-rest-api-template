package com.template.newtrackon.model;

import java.io.Serializable;

public class TrackerInfo implements Serializable {

    private Long id;
    private String url;

    public TrackerInfo() {
    }

    public TrackerInfo(Long id, String url) {
        this.id = id;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "TrackerInfo{" +
                "id=" + id +
                ", url='" + url + '\'' +
                '}';
    }
}

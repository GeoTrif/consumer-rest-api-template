package com.template.newtrackon.restcontroller;

import com.template.newtrackon.client.NewTrackonClient;
import com.template.newtrackon.model.TrackerInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NewTrackonRestController {

    private static final String NEW_TRACKON_REST_ENDPOINT = "/start-new-trackon";

    private final NewTrackonClient newTrackonClient;

    public NewTrackonRestController(NewTrackonClient newTrackonClient) {
        this.newTrackonClient = newTrackonClient;
    }

    @GetMapping(NEW_TRACKON_REST_ENDPOINT)
    public List<TrackerInfo> callNewTrackonApiAndRetrieveTrackerUrls() {
        return newTrackonClient.getTrackersPayload();
    }
}

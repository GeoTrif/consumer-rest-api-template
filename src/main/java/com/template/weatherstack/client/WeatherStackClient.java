package com.template.weatherstack.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.template.weatherstack.model.WeatherData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class WeatherStackClient {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Value("${weatherstack.api.url}")
    private String weatherStackUrl;

    public WeatherData getWeatherStackPayload(String city) {
        RestTemplate restTemplate = new RestTemplate();
        String dataPayload = restTemplate.getForObject(String.format(weatherStackUrl, city), String.class);

        WeatherData weatherData = new WeatherData();

        try {
            weatherData = objectMapper.readValue(dataPayload, WeatherData.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return weatherData;
    }
}
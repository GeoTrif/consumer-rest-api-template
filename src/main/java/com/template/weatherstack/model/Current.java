package com.template.weatherstack.model;

import java.io.Serializable;
import java.util.List;

public class Current implements Serializable {
    private String observation_time;
    private double temperature;
    private int weather_code;
    private List<String> weather_icons;
    private List<String> weather_descriptions;
    private double wind_speed;
    private double wind_degree;
    private String wind_dir;
    private double pressure;
    private double precip;
    private double humidity;
    private double cloudcover;
    private double feelslike;
    private double uv_index;
    private double visibility;
    private String is_day;

    public Current() {
    }

    public Current(String observation_time, double temperature, int weather_code, List<String> weather_icons, List<String> weather_descriptions, double wind_speed,
                   double wind_degree, String wind_dir, double pressure, double precip, double humidity, double cloudcover, double feelslike, double uv_index, double visibility, String is_day) {
        this.observation_time = observation_time;
        this.temperature = temperature;
        this.weather_code = weather_code;
        this.weather_icons = weather_icons;
        this.weather_descriptions = weather_descriptions;
        this.wind_speed = wind_speed;
        this.wind_degree = wind_degree;
        this.wind_dir = wind_dir;
        this.pressure = pressure;
        this.precip = precip;
        this.humidity = humidity;
        this.cloudcover = cloudcover;
        this.feelslike = feelslike;
        this.uv_index = uv_index;
        this.visibility = visibility;
        this.is_day = is_day;
    }

    public String getObservation_time() {
        return observation_time;
    }

    public void setObservation_time(String observation_time) {
        this.observation_time = observation_time;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public int getWeather_code() {
        return weather_code;
    }

    public void setWeather_code(int weather_code) {
        this.weather_code = weather_code;
    }

    public List<String> getWeather_icons() {
        return weather_icons;
    }

    public void setWeather_icons(List<String> weather_icons) {
        this.weather_icons = weather_icons;
    }

    public List<String> getWeather_descriptions() {
        return weather_descriptions;
    }

    public void setWeather_descriptions(List<String> weather_descriptions) {
        this.weather_descriptions = weather_descriptions;
    }

    public double getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(double wind_speed) {
        this.wind_speed = wind_speed;
    }

    public double getWind_degree() {
        return wind_degree;
    }

    public void setWind_degree(double wind_degree) {
        this.wind_degree = wind_degree;
    }

    public String getWind_dir() {
        return wind_dir;
    }

    public void setWind_dir(String wind_dir) {
        this.wind_dir = wind_dir;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getPrecip() {
        return precip;
    }

    public void setPrecip(double precip) {
        this.precip = precip;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getCloudcover() {
        return cloudcover;
    }

    public void setCloudcover(double cloudcover) {
        this.cloudcover = cloudcover;
    }

    public double getFeelslike() {
        return feelslike;
    }

    public void setFeelslike(double feelslike) {
        this.feelslike = feelslike;
    }

    public double getUv_index() {
        return uv_index;
    }

    public void setUv_index(double uv_index) {
        this.uv_index = uv_index;
    }

    public double getVisibility() {
        return visibility;
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public String getIs_day() {
        return is_day;
    }

    public void setIs_day(String is_day) {
        this.is_day = is_day;
    }

    @Override
    public String toString() {
        return "Current{" +
                "observation_time='" + observation_time + '\'' +
                ", temperature=" + temperature +
                ", weather_code=" + weather_code +
                ", weather_icons=" + weather_icons +
                ", weather_descriptions=" + weather_descriptions +
                ", wind_speed=" + wind_speed +
                ", wind_degree=" + wind_degree +
                ", wind_dir='" + wind_dir + '\'' +
                ", pressure=" + pressure +
                ", precip=" + precip +
                ", humidity=" + humidity +
                ", cloudcover=" + cloudcover +
                ", feelslike=" + feelslike +
                ", uv_index=" + uv_index +
                ", visibility=" + visibility +
                ", is_day='" + is_day + '\'' +
                '}';
    }
}

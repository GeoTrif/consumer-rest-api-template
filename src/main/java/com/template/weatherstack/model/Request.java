package com.template.weatherstack.model;

import java.io.Serializable;

public class Request implements Serializable {
    private String type;
    private String query;
    private String language;
    private String unit;

    public Request() {
    }

    public Request(String type, String query, String languange, String unit) {
        this.type = type;
        this.query = query;
        this.language = languange;
        this.unit = unit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Request{" +
                "type='" + type + '\'' +
                ", query='" + query + '\'' +
                ", languange='" + language + '\'' +
                ", unit='" + unit + '\'' +
                '}';
    }
}

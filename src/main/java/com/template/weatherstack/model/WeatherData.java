package com.template.weatherstack.model;

import java.io.Serializable;

public class WeatherData implements Serializable {
    private Request request;
    private Location location;
    private Current current;

    public WeatherData() {
    }

    public WeatherData(Request request, Location location, Current current) {
        this.request = request;
        this.location = location;
        this.current = current;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    @Override
    public String toString() {
        return "WeatherData{" +
                "request=" + request +
                ", location=" + location +
                ", current=" + current +
                '}';
    }
}

package com.template.weatherstack.restcontroller;

import com.template.weatherstack.client.WeatherStackClient;
import com.template.weatherstack.model.WeatherData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherStackRestController {

    // Url to the weather API: https://weatherstack.com/quickstart
    private static final String WEATHER_API_REST_ENDPOINT = "/start-weather-stack";
    private static final String CITY_PARAM_NAME = "city";

    private WeatherStackClient weatherStackClient;

    public WeatherStackRestController(WeatherStackClient weatherStackClient) {
        this.weatherStackClient = weatherStackClient;
    }

    @GetMapping(WEATHER_API_REST_ENDPOINT)
    public WeatherData getWeatherStackData(@RequestParam(CITY_PARAM_NAME) String city) {
        WeatherData data = weatherStackClient.getWeatherStackPayload(city);

        System.out.println(data);

        return data;
    }
}
